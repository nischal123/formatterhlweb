'use strict';
module.exports = function(app) {
  var model = require('../controller/modelController');

  // Routes

  app.route('/hlweb/')
     .get(model.list_tables);


  app.route('/hlweb/:model')
    .get(model.update_model);
   
   app.route('/hlweb/:model/:pId')
    .get(model.read_model);
};