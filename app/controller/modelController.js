'use strict';
var DataModel =require('../model/dataModel');
var DataFormatter = require('../service/dataFormatter');

exports.update_model =  function(req, res) {
    
  DataModel.getAllModelObjects(req.params.model,async function(err, projectObj) {
    if (err)
      res.send(err);   


    var promises = projectObj.map(proj => {
        return new Promise((resolve) => {
            let data = proj;

            DataFormatter.formatData(data, function(err, formattedProjectData,updateStatus) {
                if (err)  console.log(err);
                // only update if change encountered
                if(updateStatus=="false") {
                  resolve();
                  return;
                }
                DataModel.updateById(req.params.model,data.Id, formattedProjectData, function(err, projectObj) {
                    if (err) console.log(err);
                    console.log("Updated="+data.Id);
                    resolve();
                  });
              });
            }); 
        });

    await Promise.all(promises);
    console.log("complete");
    res.json("Update Complete");
    

    });

 
};


exports.read_model = function(req, res) {
  DataModel.getDataById(req.params.model,req.params.pId, function(err, projectObj) {
    if (err)  res.send(err); 
    
     res.set('Content-Type', 'text/html');
    // res.json((projectObj));
    res.send(JSON.stringify(projectObj));
  });
};


exports.list_tables = function(req, res) {
  DataModel.getTablesList(function(err, list) {
    if (err)  res.send(err); 
    
    var html="";
    for(var items of list){
      html+="<a href='"+items['Tables_in_staging'] +"'>"+items['Tables_in_staging'] +"</a><br>";
    }
    res.send(html);
  });
};


