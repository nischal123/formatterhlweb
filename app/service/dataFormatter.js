'user strict';
const config = require('../../config/configLoader');

var DataFormatter = {};
var dataObj;
//loaded from config file > config.json
const defaultFields = config.dataModelUpdates;

var DataModel = function (defFields, currentModelField) {

    for (var val of defFields) {
        if (currentModelField.includes(val))
            this[val] = "";
    }

};


DataFormatter.formatData = function (modelObj, result) {
    var currentFields = Object.keys(modelObj);
    dataObj = new DataModel(defaultFields, currentFields);
    dataObj.isUpdate = "false";
    dataObj.counter = 0;

    Object.keys(dataObj).forEach(function (key) {
        if (key == "isUpdate" || key == "counter") return;
        var val = String(modelObj[key]);
        val = formatForLocation1(val);   // checks for string like "Lat 23? / Long 45?"
        val = formatForLocation2(val);  // checks for string like  "(17?30'S, 166?E) "
        val = formatForLocation3(val);  // checks for string like  "(17<sup>?</sup>30'S, 166<sup>?</sup>E) "       
        val = formatForMMRange(val);  // runoff ?1mm, > 1 ? 5mm, > 5 ? 10 mm and > 10 mm, and recorded 
        val = formatForRange(val);      //checks for range  "56?  45"
        val = formatForUsedTo(val);  //used to ?infill' ... ?Nature dsa dsd'
        val = formatForNumDegField(val); //90? rectangular and 120? V 
        val = formatForAreaDashBlack(val);// Emerald Irrigation Area ? Black Farm
        val = formatForAreaDashBlack(val); // for recurring texts       
        val = formatForDoubleQuoteCustom(val); // custom bush mark 1
        val = formatForDoubleQuote(val);  //?reasonably permeable?       
        val = formatForStartColon(val);// : References
        val = formatForULQuestRemoval(val);  // </ul>?"
        val = formatForInveredComma(val); // Cecchi?s         
        val = formatForArrow(val); // >> insert for ?
        val = formatForAlphaBeta(val); // alpha beta generator
        val = formatForMU(val); // mu converot ug/l
        val = formatForMUbeforemeter(val); // mu converot ug/l
        val = formatFordegreeCelcius(val); // 45<sup>?</sup>C
        val = formatFordegreeCelciusSup(val); 
        val = formatForAtmosDegree(val); //NO<sub>3</sub><sup>?</sup>
        val = formatForTheta(val); // ?<sub>v</sub>
        val = formatForTrademark(val); //  ThetaProbes®
        val = formatForNegPower(val); // t ha?1
        val = formatForDivision(val); //
        val = formatForDivision(val); // for recurring
        val = formatForLessThanEquals(val);//≤0.06 t/ha/y
        val = formatForDoubleQuoteCustomRemaining(val);
        dataObj[key] = val;

    });

    var updateStatus = dataObj.isUpdate;

    //if (updateStatus == "true")
    //    console.log(modelObj.Id + "==??=" + updateStatus + " >>>count==" + dataObj.counter);


    delete dataObj.isUpdate;
    delete dataObj.counter;

    result(null, dataObj, updateStatus);
};


function formatForLocation1(data) {
    var formattedData = data.replace(/L(?:at|ong)\s*[0-9]{1,4}\?/gi, degreeConvertor);
    return formattedData;
}


function formatForLocation2(data) {
    var formattedData = data.replace(/\([0-9.]+\?([0-9]+\')*[NEWS]\s*\,\s*[0-9.]+\?([0-9]+\')*[NEWS]\)/gi, degreeConvertor);
    return formattedData;
}


function formatForLocation3(data) {
    var formattedData = data.replace(/L(?:at|ong)\s*[0-9]+[<]sup[>]\?[<]\/sup[>]/gi, degreeConvertorSup);
    return formattedData;
}

function formatForAtmosDegree(data) {
    var formattedData = data.replace(/(NO[<]sub[>]3[<]\/sub[>])+[<]sup[>]\?[<]\/sup[>]/gi, degreeConvertorSup);
    return formattedData;
}

function formatFordegreeCelcius(data) {
    var formattedData = data.replace(/[0-9.]+\?C/gi, degreeConvertor);
    return formattedData;
}

function formatFordegreeCelciusSup(data) {
    var formattedData = data.replace(/[0-9.]+[<]sup[>]\?[<]\/sup[>]\s*C/gi, degreeConvertorSup);
    return formattedData;
}

function formatForMMRange(data) {
    var formattedData = data.replace(/(?:runoff|[0-9.]+)(\s)*\?(\s)*[0-9.]+(\s)*mm/gi, lteConvertor);
    return formattedData;
}


function formatForLessThanEquals(data) {
    var formattedData = data.replace(/\?0.06 t\/ha\/y/gi, lteConvertor);
    return formattedData;
}

function formatForRange(data) {
    var formattedData = data.replace(/[0-9.]+(\s)*\?(\s)*[0-9.]+/gi, dashConvertor);
    return formattedData;
}


function formatForUsedTo(data) {
    var formattedData = data.replace(/\s*\?([A-Za-z-_0-9\s\.]+)\'/gi, quoteConvertor);
    return formattedData;
}


function formatForEmpty(data) {
    var formattedData = data.replace(/\.\s*\?*[A-Za-z0-9]/gi, emptyConvertor);
    return formattedData;
}


function formatForNumDegField(data) {
    var formattedData = data.replace(/[0-9]+\?\s*(?:rec|Vee|V-n)/gi, degreeConvertor);
    return formattedData;
}


function formatForDoubleQuote(data) {
    var formattedData = data.replace(/\?(\w+)\s*(\w+)\?/gi, doubleQuoteConvertor);
    return formattedData;
}


function formatForDoubleQuoteCustom(data) {
    var formattedData = data.replace(/\?(?:Bush, Mark I|Ott XX|Pyrox-Summer|A|B|C|D|Old Hidden Vale)\?/gi, doubleQuoteConvertor);
    return formattedData;
}
//?Proceedings of the 5
function formatForDoubleQuoteCustomRemaining(data) {
    var formattedData = data.replace(/\?Proceedings of the 5/gi, quoteConvertor);
    return formattedData;
}


//formatForAreaDashBlack("Stanthorpe ? Harslett's")

function formatForAreaDashBlack(data) {
    var fromval = "Area|east|Cl|procedures|mean|Reef|Project|Stanthorpe|Dec|May|Granodiorite|lands|studies|upper load|systems|zero|tillage|Project|cover|treatments|\\'Bare\\'|\\'High cover\\'|studies|Industries|logger|September|Sugarcane|Comparison|Period|(available\\))|(barley\\))|hill|Theodore|brief|Assessment|treatments|Douglas|Nov|Control|Charleville" +
        "|1984|1985|1986|1987|1988|1989|1990|1991|1992|1993|June|April|4400 ha|ammonium|affect soil erosion|Greenmount|rainfall \\? runoff|ranging from|December|[<]em[>]Acacia harpophylla[<]\/em[>]|fallax|pl|Education Package|concentration|Grazing Systems";
    var toval = 'Black|west|movement|Johnstone|variance|Wet|Penrose|Paschendaele|Girraween|derived|May|June|ACTFR|Kairi|lower load|na|Conserving|tillage|stubble|Greenmount|Greenwood|its|sprayed|untreated|[0-9.]+|for|March|Farm|N\/A|Stage|that|Brigalow|furrow|Capella|Daly|sorghum|no|Apr|Croxdale|summer|\\"*S' + "|Harslett" +
        "|August|wheat|peas|barley|October|197000 ha|N\\, nitrate|in southern New|A study of soil erosion on a Vertosol|change in soil|7 to 40|[<]em[>]Euclyptus brownii[<]\/em[>]|[<]em[>]Bothrichloa ewartiana[<]\/em[>]|pluviometer|Workshop Notes|cover relationships|Mt Mort\. Silburn";

    var replaceval = '(?:' + fromval + ')\\s*\\?\\s*(?:' + toval + ')';
    var re = new RegExp(replaceval, "gi");
    var formattedData = data.replace(re, dashConvertor);
    return formattedData;
}


function formatForStartColon(data) {
    var formattedData = data.replace(/^(\s)*:(\s)*/gi, emptyColon);
    return formattedData;
}

function formatForULQuestRemoval(data) {
    var formattedData = data.replace(/[<]\/ul[>]\?/gi, emptyQuestion);
    return formattedData;
}


function formatForInveredComma(data) {
    var formattedData = data.replace(/(\w+)\?(s)/gi, invertedComma);
    return formattedData;
}

function formatForArrow(data) {
    var formattedData = data.replace(/(\s*)\?\s+(?:Areas|Medium|Bare|Fin|No|Moderate)/gi, arrowConvertor);
    return formattedData;
}


function formatForAlphaBeta(data) {
    var formattedData = data.replace(/concentrations\s\?,\s*\?\s+and/gi, alphaBetaConvertor);
    return formattedData;
}


function formatForMU(data) {
    var formattedData = data.replace(/\?g(:?\/L|\sN\/L)/gi, muConvertor);
    return formattedData;
}


function formatForMUbeforemeter(data) {
    var formattedData = data.replace(/[0-9.]+\s*\?m/gi, muConvertor);
    return formattedData;
}




function formatForTheta(data) {
    var formattedData = data.replace(/\?[<]sub[>]v[<]\/sub[>]/gi, thetaConvertor);
    return formattedData;

}

function formatForTrademark(data) {
    var formattedData = data.replace(/(?:ThetaProbes)\?/gi, trademarkConvertor);
    return formattedData;

}

function formatForNegPower(data) {
    var formattedData = data.replace(/[<]sup[>]\?[0-9.]+[<]\/sup[>]/gi, dashConvertor);
    return formattedData;

}

function formatForDivision(data) {
    var formattedData = data.replace(/(:?([<]sub[>]event[<]\/sub[>])|ha|([<]sub[>]bedload[<]\/sub[>]))\s*\?\s*(:?1[0]+|ha)/gi, divisionConvertor);
    return formattedData;
}


// core replace functions

function updateFlag(val) {
    console.log(val);
    dataObj.counter += 1;
    dataObj.isUpdate = "true";
}

function muConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "&#956;");
}

function thetaConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "&#952;");
}


function trademarkConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "&#174;");
}



function divisionConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "&#247;");
}

function alphaBetaConvertor(val) {
    updateFlag(val);
    return val.replace(/\?,\s*\?/gi, "&#945;, &#946;");
}

function arrowConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "»");
}

function degreeConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "<sup>0</sup>");
}

function degreeConvertorSup(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "0");
}

function doubleQuoteConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, '"');
}

function dashConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "-");
}


function lteConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "&#8804;");
}

function repeatConvertor(val) {
    updateFlag(val);
    return val.replace(/\?+/gi, "?");
}

function quoteConvertor(val) {
    updateFlag(val);
    return val.replace(/\?/gi, "'");
}

function emptyQuestion(val) {
    updateFlag(val);
    return val.replace(/\?(\s)*/gi, "");
}

function emptyColon(val) {
    updateFlag(val);
    return val.replace(/:(\s)*/gi, "");
}

function invertedComma(val) {
    updateFlag(val);
    return val.replace(/\?/g, "\'");
}

module.exports = DataFormatter;