'user strict';

var mysql = require('mysql');

//local mysql db connection
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'staging'
});

connection.connect(function(err) {
	try{
		if (err) throw err;
	}catch(e){
		console.log(e.toString());
	}
    
});

module.exports = connection;