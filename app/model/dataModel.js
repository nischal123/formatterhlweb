'user strict';
var sql = require('./db.js');

//Project object constructor
var DataModel = function(data){
    
    Object.keys(data).forEach(function(key) {
        var val = String(data[key]);
        this[key] = val;
    }); 
    
    
  
};

DataModel.getDataById = function getDataById(table,pId, result) {
        sql.query("Select * from "+table+" where id = ? ", pId, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

DataModel.getAllModelObjects = function getAllModelObjects(table,result) {
        sql.query("Select * from "+table, function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{ 

                 result(null, res);
                }
            });   
};

DataModel.updateById = function(table,id, proj, result){
    

    sql.query("UPDATE "+table+" SET ?  WHERE id = ?", [proj, id], function (err, res) {
            if(err) {
                console.log("error: ", err);
                    result(null, err);
                }
            else{   
                result(null, res);
                    }
                }); 
};


DataModel.getTablesList = function(result){

    sql.query("show tables in staging", function (err, res) {
            if(err) {
                console.log("error: ", err);
                    result(null, err);
                }
            else{   
                result(null, res);
                    }
                }); 
};



module.exports= DataModel;