const fs = require('fs');
const path = require('path');
const NODE_ENV = process.env.NODE_ENV || "development";
let configBuffer = fs.readFileSync(path.resolve(__dirname, 'config.json'), 'utf-8');


let config = JSON.parse(configBuffer);
module.exports = config;